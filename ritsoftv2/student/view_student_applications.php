<?php
  include("includes/header.php");
  include("includes/sidenav.php");
  include("includes/connection.php");
  include("includes/test.php");

  $add_no=$_SESSION['admissionno'];


  ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
  	$(document).ready(function($) {
  		$('.DataTable').DataTable();
  	});

  	function setid(a) {
  		console.log(a);
  		document.getElementById('reg_id').value = a;

  	}
  </script>

  </head>
  <body>

<form class="" action="stud_app_withdraw.php" method="post">


  <div id="page-wrapper" style="height: auto;" >

  	<div class="container-fluid">
  		<div class="row">
  			<div class="col-lg-12">

  				<h3 class="page-header"> <b>APPLICATIONS VIEW</b>

  				</h3>
  			</div>
  			<!-- /.col-lg-12 -->
  		</div>
  		<!-- /.row -->
  	</div>

    <?php
    $query=mysql_query("select distinct(name),admissionno,courseid from stud_details join current_class where admissionno='$add_no'");
    while($row =mysql_fetch_assoc($query))
    	{
    		$name=$row['name'];
    		$adm=$row['admissionno'];
    		$course=$row['courseid'];


    	}
    ?>
    <div class="table-responsive">
    		<table   class="table table-hover table-bordered" >
    			<tr>
    				<th style="text-align: center;">NAME</th>
    				<th style="text-align: center;">ADMISSION NO</th>
    				<th style="text-align: center;">COURSE</th>

    			</tr>
    			<tr align="center">

    				<td align="center"><?php  echo $name;?></td>
    				<td align="center"><?php  echo $adm;?></td>
    				<td align="center"><?php  echo $course;?></td>

    			</table>
    		</div>


        <br>




  	<ul class="nav nav-tabs">

  		<li class="active"><a data-toggle="tab"  href="#menu1">Approved</a></li>
  		<li><a data-toggle="tab" href="#menu2">Pending</a></li>
  		<li><a data-toggle="tab" href="#menu3">Rejected</a></li>
      <li><a data-toggle="tab" href="#menu4">Withdrawn</a></li>
  	</ul>

  	<div class="tab-content">
  		<div id="menu1" class="tab-pane fade in active">
  			<h4>Approved Requests</h4>
  			<div class="table-responsive">
  				<table   class="table table-hover table-bordered" >
  					<tr>
                  <!-- <th style="text-align: center; text-transform: ;">Application ID</th> -->
                  <th style="text-align: center;">Applied Date</th>
                  <th style="text-align: center;">Subject Line</th>
                  <th style="text-align: center;">SA Approved On</th>
                  <th style="text-align: center;">HOD Approved On</th>
                  <th style="text-align: center;">Principal Approved On</th>
                  <th style="text-align: center;">Office Approved On</th>

            </tr>

            <?php
//reading student application details
            $resul=mysql_query("select * from student_application where status=5 and admissionno='$adm' order by app_id desc",$con);


            while($dat=mysql_fetch_assoc($resul))
            {
                  $app_id=$dat["app_id"];
                  $applieddate=$dat["applieddate"];
                  $subject=$dat["subject"];
                  $sa_approved_date=$dat["sa_approved_date"];
                  $hod_approved_date=$dat["hod_approved_date"];
                  $principal_approved_date=$dat["principal_approved_date"];
                  $office_approved_date=$dat["office_approved_date"];
            ?>

            <tr align="center">

                  <!-- <td align="center"><?php  echo $app_id;?></td> -->
                  <td align="center"><?php  echo $applieddate;?></td>
                  <td align="center"><?php  echo $subject;?></td>
                  <td align="center"><?php  echo $sa_approved_date;?></td>
                  <td align="center"><?php  echo $hod_approved_date; ?></td>
                  <td align="center"><?php  echo $principal_approved_date;?></td>
                  <td align="center"><?php  echo $office_approved_date; ?></td>
                  <!-- <td align="center" style="color: green;">



              </td> -->
            </tr>
          <?php } ?>
        </table>
      </div>
    </div>



    <div id="menu2" class="tab-pane fade">
     <h4>Pending Requests</h4>
     <div class="table-responsive">
      <table   class="table table-hover table-bordered" >
       <tr>

       <!-- <td align="center"><?php  echo $serial_no;?></td> -->
       <!-- <th style="text-align: center; text-transform: ;">Application ID</th> -->
       <th style="text-align: center;">Applied Date</th>
       <th style="text-align: center;">Subject Line</th>
       <!-- <th style="text-align: center;">Expected Date</th> -->
       <th style="text-align: center;">SA Approved On</th>
       <th style="text-align: center;">HOD Approved On</th>
       <th style="text-align: center;">Principal Approved On</th>
       <th style="text-align: center;">Office Approved On</th>
       <th style="text-align: center;">Pending At</th>
       <th style="text-align: center;">Withdraw</th>

      </tr>




      <?php
//reading student application details
      $resul=mysql_query("select * from student_application where status in (1,2,3,4) and admissionno='$adm' order by app_id desc",$con);

      while($dat=mysql_fetch_assoc($resul))
      {
            $app_id=$dat["app_id"];
            $applieddate=$dat["applieddate"];
            $subject=$dat["subject"];
            $expected_date=$dat["expected_date"];
            $status=$dat["status"];
            if($status==1)
            {
              $sa_approved_date='Not Approved';
              $hod_approved_date='Not Approved';
              $principal_approved_date='Not Approved';
              $office_approved_date='Not Approved';
              $pendingAt='Staff Advisor';
            }
            else if($status==2)
            {
              $sa_approved_date=$dat["sa_approved_date"];
              $hod_approved_date='Not Approved';
              $principal_approved_date='Not Approved';
              $office_approved_date='Not Approved';
              $pendingAt='HOD';
            }
            else if($status==3)
            {
              $sa_approved_date=$dat["sa_approved_date"];
              $hod_approved_date=$dat["hod_approved_date"];
              $principal_approved_date='Not Approved';
              $office_approved_date='Not Approved';
              $pendingAt='Principal';
            }
            else
            {
              $sa_approved_date=$dat["sa_approved_date"];
              $hod_approved_date=$dat["hod_approved_date"];
              $principal_approved_date=$dat["principal_approved_date"];
              $office_approved_date='Not Approved';
              $pendingAt='Office';
            }

      ?>
       <tr align="center">

        <!-- <td align="center"><?php  echo $app_id;?></td> -->
        <td align="center"><?php  echo $applieddate;?></td>
        <td align="center"><?php  echo $subject;?></td>
        <!-- <td align="center"><?php  echo $expected_date;?></td> -->
        <td align="center"><?php  echo $sa_approved_date; ?></td>
        <td align="center"><?php  echo $hod_approved_date; ?></td>
        <td align="center"><?php  echo $principal_approved_date; ?></td>
        <td align="center"><?php  echo $office_approved_date; ?></td>
        <td align="center" ><?php  echo $pendingAt; ?></td>



<div class="container">

  <!-- Trigger the modal with a button -->
  <td align="center" ><a href="#" onclick="setid(<?php echo $app_id;?>)"
													data-toggle="modal" data-target="#myModal"
													><font color="red">Withdraw</font></a>
</td>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">
						<form method="post" action="stud_app_withdraw.php">

							<div class="modal-content">
								<div class="modal-header">
									Withdraw
									<button type="button" class="close" data-dismiss="modal">&times;</button>
								</div>
								<div class="modal-body">

									<div data-role="popup" id="myPopup" class="ui-content" style="min-width:300px;">
										<?php

										?>

										<div>

											<input class="ui-accessible" type="hidden" name="reg_id" id="reg_id" >
											<label for="text" class="ui-hidden-accessible">Withdraw Reason</label>
											<textarea type="text" class="form-control" name="withdraw_reason" id="withdraw_reason" placeholder="Enter the Reason for Application Withdrawal!" required></textarea>

										</div>

									</div>


								</div>
								<div class="modal-footer">
									<input type="submit" data-inline="true" class="btn btn-primary" value="Submit" name="btn_send" id="btn_send">
								</div>
							</div>
						</form>
					</div>
				</div>
      </tr>
    <?php } ?>
  </table>
</div>
</div>


<div id="menu3" class="tab-pane fade">
 <h4>Rejected Requests</h4>


 <div class="table-responsive">
   <form class="" action="resubmit_apps.php" method="get">
  <table   class="table table-hover table-bordered" >
   <tr>
     <!-- <th style="text-align: center; text-transform: ;">Application ID</th> -->
     <th style="text-align: center;">Applied Date</th>
     <th style="text-align: center;">Subject Line</th>
     <th style="text-align: center;">Rejected By</th>
     <th style="text-align: center;">Rejected On</th>
     <th style="text-align: center;">Comment</th>
     <th style="text-align: center;">ReSubmit</th>

  </tr>

  <?php
  //reading student application details
  $resul=mysql_query("select * from student_application where status in (6,7,8,9) and admissionno='$adm' order by app_id desc",$con);


  while($dat=mysql_fetch_assoc($resul))
  {
        $app_id=$dat["app_id"];
        $applieddate=$dat["applieddate"];
        $subject=$dat["subject"];
        $status=$dat["status"];
        if($status==6)
        {
          $Rejected_date=$dat["sa_approved_date"];
          $Rejected_by='Staff Advisor';
          $comment=$dat["sa_comment"];
        }
        else if($status==7)
        {
          $Rejected_date=$dat["hod_approved_date"];
          $Rejected_by='HOD';
          $comment=$dat["hod_comment"];
        }
        else if($status==8)
        {
          $Rejected_date=$dat["principal_approved_date"];
          $Rejected_by='Principal';
          $comment=$dat["principal_comment"];
        }
        else
        {
          $Rejected_date=$dat["office_approved_date"];
          $Rejected_by='Office';
          $comment=$dat["office_comment"];
        }


  ?>
   <tr align="center">

    <!-- <td align="center"><?php  echo $app_id;?></td> -->
    <td align="center"><?php  echo $applieddate;?></td>
    <td align="center"><?php  echo $subject;?></td>
    <td align="center"><?php  echo $Rejected_by;?></td>
    <td align="center"><?php  echo $Rejected_date; ?></td>
    <td align="center"><?php  echo $comment; ?></td>
    <td align="center"><a href="resubmit_apps.php?app_id=<?php echo $app_id; ?>" target="_blank">ReSubmit</a> </td>

  </tr>
  <?php } ?>
</table>



</form>

</div>
</div>

<div id="menu4" class="tab-pane fade">
 <h4>Withdrawn Requests</h4>
 <div class="table-responsive">
  <table   class="table table-hover table-bordered" >
   <tr>


   <!-- <th style="text-align: center; text-transform: ;">Application ID</th> -->
   <th style="text-align: center;">Applied Date</th>
   <th style="text-align: center;">Subject Line</th>
   <th style="text-align: center;">Withdrawn On</th>
   <th style="text-align: center;">Reason</th>

  </tr>




  <?php
//reading student application details
  $resul=mysql_query("select * from student_application where status=10 and admissionno='$adm' order by app_id desc",$con);

  while($dat=mysql_fetch_assoc($resul))
  {
        $app_id=$dat["app_id"];
        $applieddate=$dat["applieddate"];
        $subject=$dat["subject"];
        $Withdrawn_date=$dat["withdraw_date"];
        $withdraw_reason=$dat["withdraw_reason"];

  ?>
   <tr align="center">

    <!-- <td align="center"><?php  echo $app_id;?></td> -->
    <td align="center"><?php  echo $applieddate;?></td>
    <td align="center"><?php  echo $subject;?></td>
    <td align="center"><?php  echo $Withdrawn_date;?></td>
    <td align="center"><?php  echo $withdraw_reason; ?></td>

  </tr>
<?php } ?>
</table>
</div>
</div>


</div>

</div>
</form>
</body>
</html>
